package com.app.airport.apps.dvt.com.dvtairport.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.airport.apps.dvt.com.dvtairport.R
import com.app.airport.apps.dvt.com.dvtairport.models.Flightdata
import kotlinx.android.synthetic.main.airport_list_item.view.*
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*


class FlightsAdapter (val items: List<Flightdata>?, val context: Context) : RecyclerView.Adapter<ViewHolder>(){

    override fun getItemCount(): Int {

            return items!!.size

    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.airport_list_item, parent, false))
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (items != null) {
            holder?.tvflightname?.text = items.get(position).airline.name

    holder?.tvstatus?.text = items.get(position).status.capitalize()
    holder?.tvdeptime?.text = convertDate(items.get(position).departure.scheduledTime)
            holder?.tvfno?.text = items.get(position).flight.iataNumber

    holder?.tvdestination?.text = items.get(position).arrival.iataCode


    if(items.get(position).status == "active") {
        holder?.ivstatusIcon?.setImageResource(R.drawable.green_dot_x)
    }else{
        holder?.ivstatusIcon?.setImageResource(R.drawable.red_dot_x)
    }

    }

    }
}


@SuppressLint("NewApi")
//Convert time
fun convertDate(stringDate:String): String?{


    val inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH)
    val outputFormatter = DateTimeFormatter.ofPattern("HH:mm", Locale.ENGLISH)
    val date = LocalTime.parse(stringDate, inputFormatter)
    val formattedDate = outputFormatter.format(date)
    return formattedDate

}


class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val tvflightname = view.tv_flightname
    val ivstatusIcon = view.iv_statusicon
    val tvstatus = view.tv_status
    val tvdeptime = view.tv_dtime
    val tvfno = view.tv_flightnumber
    val tvdestination = view.tv_destination

}