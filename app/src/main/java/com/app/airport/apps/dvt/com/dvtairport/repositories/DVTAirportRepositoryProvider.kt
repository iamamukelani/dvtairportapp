package com.segunfamisa.kotlin.samples.retrofit.data.kotlin

import com.app.airport.apps.dvt.com.dvtairport.interfaces.GithubApiService


object DVTAirportRepositoryProvider {

    fun provideSearchRepository(): DVTAirportRepository {
        return DVTAirportRepository(GithubApiService.Factory.create())
    }



}