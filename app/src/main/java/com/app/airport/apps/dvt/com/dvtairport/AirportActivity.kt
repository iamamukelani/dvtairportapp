package com.app.airport.apps.dvt.com.dvtairport

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import com.app.airport.apps.dvt.com.dvtairport.adapters.FlightsAdapter
import com.app.airport.apps.dvt.com.dvtairport.models.Flightdata
import com.segunfamisa.kotlin.samples.retrofit.data.kotlin.DVTAirportRepositoryProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_airport.*
import java.util.*


class AirportActivity : AppCompatActivity() {


    private var appBarLayout : AppBarLayout? = null
    private var collapsingToolbar : CollapsingToolbarLayout? = null


     var appBarExpanded : Boolean?= true

    val animals: ArrayList<String> = ArrayList()
    var flights: List<Flightdata>? = null

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    var codeIataAirport: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_airport)


        var bitmap : Bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.img_x);


        setSupportActionBar(anim_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        appBarLayout = findViewById(R.id.appbar) as AppBarLayout

        collapsingToolbar = findViewById(R.id.collapsing_toolbar) as CollapsingToolbarLayout

        //Getting the Values from the Main Acivity
        val nameAirport:String = intent.getStringExtra("nameAirport")
        codeIataAirport = intent.getStringExtra("codeIataAirport")

        collapsingToolbar!!.setTitle(nameAirport)

        appBarLayout!!.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            Log.d(AirportActivity::class.java!!.getSimpleName(), "onOffsetChanged: verticalOffset: $verticalOffset")

            //  Vertical offset == 0 indicates appBar is fully expanded.
            if (Math.abs(verticalOffset) > 200) {
                appBarExpanded = false
                invalidateOptionsMenu()
            } else {
                appBarExpanded = true
                invalidateOptionsMenu()
            }
        })


        // Creates a vertical Layout Manager
        scrollableview.layoutManager = LinearLayoutManager(this)

        //Getting the Fligths details from the Airport
        getFlights()

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //Getting Fligths from the Service Call
    fun  getFlights(){

        val repository = DVTAirportRepositoryProvider.provideSearchRepository()

        compositeDisposable.add(
                repository.getflightsData(codeIataAirport, "departure")
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->
                            Log.d("Result", "There are ${result.size} Flights in the Airport")

                            Log.d("flights", result.toString())

                            flights = result

                            scrollableview.adapter = FlightsAdapter(this!!.flights, this)

                        }, { error ->
                            error.printStackTrace()
                        })
        )

    }




    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }


}
