package com.app.airport.apps.dvt.com.dvtairport.models

import com.google.gson.annotations.SerializedName

data class Airport(@SerializedName("nameAirport") val nameAirport: String,
                   @SerializedName("codeIataAirport") val codeIataAirport: String,
                   @SerializedName("codeIcaoAirport") val codeIcaoAirport: String,
                   @SerializedName("nameCountry") val nameCountry: String,
                   @SerializedName("codeIso2Country") val codeIso2Country: String,
                   @SerializedName("distance") val distance: Double,
                   @SerializedName("codeIataCity") val  codeIataCity: String,
                   @SerializedName("latitudeAirport") val latitudeAirport: Double,
                   @SerializedName("longitudeAirport") val longitudeAirport: Double,
                   @SerializedName("gmt") val gmt: String,
                   @SerializedName("phone") val phone:String,
                   @SerializedName("nameTranslations") val nameTranslations: String)

data class AirportResult (
        val items: List<Airport>
)