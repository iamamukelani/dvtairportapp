package com.segunfamisa.kotlin.samples.retrofit.data.kotlin

import com.app.airport.apps.dvt.com.dvtairport.interfaces.GithubApiService
import com.app.airport.apps.dvt.com.dvtairport.models.Airport
import com.app.airport.apps.dvt.com.dvtairport.models.AirportResult
import com.app.airport.apps.dvt.com.dvtairport.models.Flightdata
import io.reactivex.Observable


/**
 * Repository method to access search functionality of the api service
 */
class DVTAirportRepository(val apiService: GithubApiService) {

    fun getAirports(lat: Double, lng: Double, distance: Int): io.reactivex.Observable<AirportResult>{
        return apiService.GetAirports(lat, lng ,distance)
    }

    fun getairportData(lat: Double, lng: Double, distance: Int): Observable<List<Airport>> {
        return apiService.getairportData(lat, lng ,distance)
    }

    fun getflightsData(iataCode: String,type: String): Observable<List<Flightdata>>{
        return apiService.getflightsData(iataCode, type)
    }

}