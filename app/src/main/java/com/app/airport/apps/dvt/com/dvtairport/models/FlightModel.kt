package com.app.airport.apps.dvt.com.dvtairport.models

import com.google.gson.annotations.SerializedName
//Flight Data Model
data class Flightdata(@SerializedName("type") var type: String,
                  @SerializedName("status") val status: String,
                  val departure: departure,
                  val arrival:arrival,
                  var airline: airline,
                  var flight: flight
                )

data class departure(@SerializedName("iataCode") var iataCode: String,
                        @SerializedName("icaoCode") val icaoCode: String,
                        @SerializedName("terminal") val terminal: String,
                        @SerializedName("gate") val gate: String,
                        @SerializedName("scheduledTime") val scheduledTime: String,
                        @SerializedName("estimatedTime") val estimatedTime: String,
                        @SerializedName("actualTime") val actualTime: String
                     )

data class arrival(@SerializedName("iataCode") var iataCode: String,
                   @SerializedName("icaoCode") val icaoCode: String,
                   @SerializedName("baggage") val baggage: String,
                   @SerializedName("scheduledTime") val scheduledTime: String,
                   @SerializedName("estimatedTime") val estimatedTime: String,
                   @SerializedName("actualTime") val actualTime: String
                   )

data class airline(@SerializedName("name") var name: String,
                   @SerializedName("iataCode") val iataCode: String,
                   @SerializedName("icaoCode") val icaoCode: String)

data class flight(@SerializedName("number") var number: String,
                  @SerializedName("iataNumber") val iataNumber: String,
                  @SerializedName("icaoNumber") val icaoNumber: String)


