package com.app.airport.apps.dvt.com.dvtairport

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.app.airport.apps.dvt.com.dvtairport.models.Airport
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.segunfamisa.kotlin.samples.retrofit.data.kotlin.DVTAirportRepository
import com.segunfamisa.kotlin.samples.retrofit.data.kotlin.DVTAirportRepositoryProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

private const val PERMISSION_REQUEST = 10

class MainActivity : AppCompatActivity(), OnMapReadyCallback {




    private lateinit var mMap: GoogleMap

    var source: Marker? = null
    var destination:Marker? = null



    protected var mLastLocation: Location? = null
    private var airports: List<Airport>? = null

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    private var locationName: String? = null

    private var mLocationManager: LocationManager? = null

    val compositeDisposable: CompositeDisposable = CompositeDisposable()


    lateinit var locationManager: LocationManager

    lateinit var repository: DVTAirportRepository
    private var hasGps = false
    private var hasNetwork = false
    private var locationGps: Location? = null
    private var locationNetwork: Location? = null

    private var permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager


    }


    @SuppressLint("MissingPermission")
    private fun GetLocation() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        hasGps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        hasNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        if (hasGps || hasNetwork) {

            if (hasGps) {
                Log.d("CodeAndroidLocation", "hasGps")
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 50000, 0F, object : LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        if (location != null) {
                            locationGps = location
                            latitude = locationGps!!.latitude;
                            longitude = locationGps!!.longitude;

                            GetAirports(latitude, longitude);
                            Log.d("CodeAndroidLocation", " GPS Latitude : " + locationGps!!.latitude)
                            Log.d("CodeAndroidLocation", " GPS Longitude : " + locationGps!!.longitude)
                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }

                })

                val localGpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                if (localGpsLocation != null)
                    locationGps = localGpsLocation
            }
            if (hasNetwork) {
                Log.d("CodeAndroidLocation", "hasGps")
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0F, object : LocationListener {
                    override fun onLocationChanged(location: Location?) {
                        if (location != null) {
                            locationNetwork = location
                            latitude = locationGps!!.latitude;
                            longitude = locationGps!!.longitude;

                            GetAirports(latitude, longitude);

                            Log.d("CodeAndroidLocation", " Network Latitude : " + locationNetwork!!.latitude)
                            Log.d("CodeAndroidLocation", " Network Longitude : " + locationNetwork!!.longitude)
                        }
                    }

                    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                    }

                    override fun onProviderEnabled(provider: String?) {

                    }

                    override fun onProviderDisabled(provider: String?) {

                    }

                })

                val localNetworkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                if (localNetworkLocation != null)
                    locationNetwork = localNetworkLocation
            }

            if(locationGps!= null && locationNetwork!= null){
                if(locationGps!!.accuracy > locationNetwork!!.accuracy){
                    latitude = locationGps!!.latitude;
                    longitude = locationGps!!.longitude;

                    GetAirports(latitude, longitude);

                    Log.d("CodeAndroidLocation", " Network Latitude : " + locationNetwork!!.latitude)
                    Log.d("CodeAndroidLocation", " Network Longitude : " + locationNetwork!!.longitude)
                }else{
                    latitude = locationGps!!.latitude;
                    longitude = locationGps!!.longitude;

                    GetAirports(latitude, longitude);

                    Log.d("CodeAndroidLocation", " GPS Latitude : " + locationGps!!.latitude)
                    Log.d("CodeAndroidLocation", " GPS Longitude : " + locationGps!!.longitude)
                }
            }

        } else {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
        }
    }


    private fun GetAirports(lat:Double, lng: Double){

        //Getting all Airports around you in a 100 Meters Radius
        compositeDisposable.add(
                repository.getairportData(lat, lng, 100)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe ({
                            result ->
                            Log.d("Result", "There are ${result.size} Airports around you")

                            Log.d("airports", result.toString())

                            airports = result

                            result.forEach {
                                airport: Airport ->

                                val currentLocation = LatLng(airport.latitudeAirport, airport.longitudeAirport)
                                mMap.addMarker(MarkerOptions().position(currentLocation).title(airport.codeIcaoAirport))
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation))

                            }


                        }, { error ->
                            error.printStackTrace()
                        })
        )

    }


    private fun checkPermission(permissionArray: Array<String>): Boolean {
        var allSuccess = true
        for (i in permissionArray.indices) {
            if (checkCallingOrSelfPermission(permissionArray[i]) == PackageManager.PERMISSION_DENIED)
                allSuccess = false
        }
        return allSuccess
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST) {
            var allSuccess = true
            for (i in permissions.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    allSuccess = false
                    val requestAgain = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && shouldShowRequestPermissionRationale(permissions[i])
                    if (requestAgain) {
                        Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Go to settings and enable the permission", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            if (allSuccess)
                GetLocation()

        }
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("NewApi")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        repository = DVTAirportRepositoryProvider.provideSearchRepository()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission(permissions)) {
                GetLocation()
            } else {
                requestPermissions(permissions, PERMISSION_REQUEST)
            }
        } else {
            GetLocation()
        }

        // If marker source is clicked
        mMap.setOnMarkerClickListener { marker ->

            if(marker.title != null || marker.title == ""){

                airports!!.forEach { airport: Airport ->


                    if(airport.codeIcaoAirport == marker.title){

                        val airportIntent = Intent(this, AirportActivity::class.java)
                        // To pass any data to next activity
                        airportIntent.putExtra("codeIataAirport", airport.codeIataAirport)
                        airportIntent.putExtra("nameAirport", airport.nameAirport)
                        airportIntent.putExtra("nameCountry", airport.nameCountry)
                        airportIntent.putExtra("distance", airport.distance)

                        // start your next activity
                        startActivity(airportIntent)

                    }

                }


            }

            true
            }
    }




    companion object {

        private val TAG = "MainActivity"

        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
        private val MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION = 100
    }


    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }



}
