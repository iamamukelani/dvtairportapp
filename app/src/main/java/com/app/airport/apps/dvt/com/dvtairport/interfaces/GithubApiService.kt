package com.app.airport.apps.dvt.com.dvtairport.interfaces

import com.app.airport.apps.dvt.com.dvtairport.models.Airport
import com.app.airport.apps.dvt.com.dvtairport.models.AirportResult
import com.app.airport.apps.dvt.com.dvtairport.models.Flightdata
import retrofit2.http.GET


interface GithubApiService {


    //Creating End Points for the Airpoer Service
    @retrofit2.http.GET("nearby?key=f41141-23fb7a")
    fun GetAirports(@retrofit2.http.Query("lat") lat: Double,
                    @retrofit2.http.Query("lng") lng: Double,
                    @retrofit2.http.Query("distance") distance: Int): io.reactivex.Observable<AirportResult>

    @GET("nearby?key=f41141-23fb7a")
    fun getairportData(@retrofit2.http.Query("lat") lat: Double,
                    @retrofit2.http.Query("lng") lng: Double,
                    @retrofit2.http.Query("distance") distance: Int): io.reactivex.Observable<List<Airport>>

    @GET("timetable?key=f41141-23fb7a")
    fun getflightsData(@retrofit2.http.Query("iataCode") iataCode: String,
                       @retrofit2.http.Query("type") type: String): io.reactivex.Observable<List<Flightdata>>


    /**
     * Companion object for the factory
     */
    companion object Factory {
        fun create(): GithubApiService {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create())
                    .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                    .baseUrl("http://aviation-edge.com/v2/public/")
                    .build()

            return retrofit.create(GithubApiService::class.java);
        }
    }
}